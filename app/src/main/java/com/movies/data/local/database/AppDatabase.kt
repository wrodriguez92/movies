package com.movies.data.local.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.movies.data.local.daos.MovieDao
import com.movies.data.models.MoviePlayingNow
import com.movies.data.models.MovieDetailResponse
import com.movies.data.models.MoviePopular
import com.movies.utils.converters.IntArrayListConverter
import com.movies.utils.converters.StringArrayListConverter

@Database(
    entities = [MoviePlayingNow::class, MoviePopular::class, MovieDetailResponse::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(value = [(IntArrayListConverter::class), (StringArrayListConverter::class)])
abstract class AppDatabase : RoomDatabase() {

    abstract fun movieDao(): MovieDao

    companion object {
        @Volatile
        private var instance: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase =
            instance ?: synchronized(this) {
                instance ?: buildDatabase(
                    context
                ).also { instance = it }
            }

        private fun buildDatabase(appContext: Context) =
            Room.databaseBuilder(appContext, AppDatabase::class.java, DatabaseEnum.movies.name)
                .fallbackToDestructiveMigration()
                .build()
    }
}

enum class DatabaseEnum {
    movies
}