package com.movies.data.local.daos

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.movies.data.models.MoviePlayingNow
import com.movies.data.models.MovieDetailResponse
import com.movies.data.models.MoviePopular

@Dao
interface MovieDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPlayingNow(movies: List<MoviePlayingNow>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPopular(movies: List<MoviePopular>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(movie: MovieDetailResponse)

    @Query("SELECT * FROM movies_now_playing")
    fun getMoviesPlayingNow(): LiveData<List<MoviePlayingNow>>

    @Query("SELECT * FROM movies_popular")
    fun getMoviesPopular(): LiveData<List<MoviePopular>>

    @Query("SELECT * FROM movies_now_playing WHERE id = :mId")
    fun getMovie(mId: Int): LiveData<MoviePlayingNow>

    @Query("SELECT * FROM movie_detail WHERE id = :id")
    fun getMovieDetail(id: Int): LiveData<MovieDetailResponse>
}