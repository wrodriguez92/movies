package com.movies.data.remote

import com.movies.data.models.MovieDetailResponse
import com.movies.data.models.response.MoviePopularResponse
import com.movies.data.models.response.MovieResponse
import com.movies.data.models.response.MovieVideosResponse
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieService {

    @GET("movie/now_playing")
    suspend fun getMoviesPlayingNow(
        @Query("page") page: Int
    ): Response<MovieResponse>

    @GET("movie/popular")
    suspend fun getMoviesPopular(
        @Query("page") page: Int
    ): Response<MoviePopularResponse>

    @GET("movie/{movieId}")
    suspend fun getMovie(
        @Path("movieId") id: Int
    ): Response<MovieDetailResponse>

    @GET("movie/{id}/videos")
    fun getMovieVideos(@Path("id") id: Int): Call<MovieVideosResponse>
}