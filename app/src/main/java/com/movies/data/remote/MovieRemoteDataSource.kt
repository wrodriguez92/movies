package com.movies.data.remote

import javax.inject.Inject

class MovieRemoteDataSource @Inject constructor(
    private val movieService: MovieService
): BaseDataSource() {

    suspend fun getMoviesPlayingNow(page: Int) = getResult { movieService.getMoviesPlayingNow(page = page) }

    suspend fun getMoviesPopular(page: Int) = getResult { movieService.getMoviesPopular(page = page) }

    suspend fun getMovie(id: Int) = getResult { movieService.getMovie(id = id) }

}