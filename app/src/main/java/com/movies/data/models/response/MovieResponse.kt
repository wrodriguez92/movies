package com.movies.data.models.response

import com.google.gson.annotations.SerializedName
import com.movies.data.models.MoviePlayingNow
import com.movies.data.models.MoviePopular
import com.movies.data.models.Video

data class MovieResponse(
    var page: Int,
    val results: List<MoviePlayingNow>,
    val total_pages: Int,
    val total_results: Int
)

data class MoviePopularResponse(
    var page: Int,
    val results: List<MoviePopular>,
    val total_pages: Int,
    val total_results: Int
)

data class MovieVideosResponse (
    @SerializedName("id")
    val id: Int,
    @SerializedName("results")
    val videos: ArrayList<Video>
)