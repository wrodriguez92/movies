package com.movies.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.movies.constants.ApiConstants

@Entity(tableName = "movies_now_playing")
data class MoviePlayingNow(
    @PrimaryKey
    val id: Int,
    val adult: Boolean,
    val backdrop_path: String?,
    val genre_ids: List<Int>,
    val original_language: String,
    val original_title: String,
    val overview: String,
    val popularity: Float,
    val poster_path: String?,
    val release_date: String,
    val title: String,
    val video: Boolean,
    val vote_average: Float,
    val vote_count: Int,
    var page: Int
) {
    fun getPosterUrl() = "${ApiConstants.base_image_url}$poster_path"
}
