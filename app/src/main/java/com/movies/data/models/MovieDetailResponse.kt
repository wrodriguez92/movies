package com.movies.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.movies.constants.ApiConstants

@Entity(tableName = "movie_detail")
data class MovieDetailResponse(
    @PrimaryKey val id: Int,
    val original_title: String,
    val overview: String,
    val vote_average: Float,
    val poster_path: String?,
){
    fun getPosterUrl() = "${ApiConstants.base_image_url}$poster_path"
}
