package com.movies.data.repository

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.movies.data.local.daos.MovieDao
import com.movies.data.models.Video
import com.movies.data.models.response.MovieVideosResponse
import com.movies.data.remote.MovieRemoteDataSource
import com.movies.data.remote.MovieService
import com.movies.utils.PlayerUtils
import com.movies.utils.performGetOperation
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.util.concurrent.Executors
import javax.inject.Inject

class MovieRepository @Inject constructor(
    private val remoteDataSource: MovieRemoteDataSource,
    private val localDataSource: MovieDao,
    private val movieService: MovieService,
    private val application: Application
) {

    fun getMovie(id: Int) = performGetOperation(
        databaseQuery = { localDataSource.getMovieDetail(id) },
        networkCall = { remoteDataSource.getMovie(id) },
        saveCallResult = { localDataSource.insert(it) }
    )

    fun getMoviesPlayingNow(page: Int) = performGetOperation(
        databaseQuery = { localDataSource.getMoviesPlayingNow() },
        networkCall = { remoteDataSource.getMoviesPlayingNow(page) },
        saveCallResult = {
            val mList = it.results
            mList.forEach { mMovie -> mMovie.page = page }
            localDataSource.insertPlayingNow(mList)
        }
    )

    fun getMoviesPopular(page: Int) = performGetOperation(
        databaseQuery = { localDataSource.getMoviesPopular() },
        networkCall = { remoteDataSource.getMoviesPopular(page) },
        saveCallResult = {
            val mList = it.results
            mList.forEach { mMovie -> mMovie.page = page }
            localDataSource.insertPopular(mList)
        }
    )

    fun requestMovieVideos(id: Int): LiveData<ArrayList<Video>> {
        val mutableData = MutableLiveData<ArrayList<Video>>()
        movieService.getMovieVideos(id).enqueue(object : Callback<MovieVideosResponse> {
            override fun onResponse(
                call: Call<MovieVideosResponse>,
                response: Response<MovieVideosResponse>
            ) {
                if(response.isSuccessful){
                    response.body()?.let {
                        mutableData.value = it.videos
                        Executors.newSingleThreadExecutor().execute {
                            PlayerUtils.requestVideoPlaybackData(application, mutableData)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<MovieVideosResponse>, t: Throwable) {
                Timber.d(t)
            }

        })
        return mutableData
    }

}