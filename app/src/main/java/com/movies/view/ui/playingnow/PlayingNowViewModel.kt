package com.movies.view.ui.playingnow

import androidx.lifecycle.ViewModel
import com.movies.data.repository.MovieRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PlayingNowViewModel @Inject constructor(
    private val movieRepository: MovieRepository
) : ViewModel() {
    private var moviePage: Int = 1
    val movies = movieRepository.getMoviesPlayingNow(moviePage)
    val movies2 = movieRepository.getMoviesPlayingNow(moviePage+1)
    val movies3 = movieRepository.getMoviesPlayingNow(moviePage+2)
    val movies4 = movieRepository.getMoviesPlayingNow(moviePage+3)

    fun getMoviesPlayingNow() {
        moviePage+=1
        movieRepository.getMoviesPlayingNow(moviePage)
    }
}