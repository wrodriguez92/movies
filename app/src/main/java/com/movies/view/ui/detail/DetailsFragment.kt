package com.movies.view.ui.detail

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.movies.R
import com.movies.data.models.MovieDetailResponse
import com.movies.data.models.Video
import com.movies.databinding.FragmentDetailsBinding
import com.movies.utils.FormatedResponse
import com.movies.view.ui.activities.PlayerActivity
import com.movies.view.ui.activities.PlayerActivity.Companion.PARAM_URL
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class DetailsFragment : Fragment() {

    private lateinit var viewModel: DetailsViewModel
    private var _binding: FragmentDetailsBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    companion object {
        fun newInstance(): DetailsFragment {
            return DetailsFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel =
            ViewModelProvider(this)[DetailsViewModel::class.java]

        _binding = FragmentDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getInt("movieId")?.let { viewModel.mediaId = it }
        viewModel.requestMediaInfo()
        viewModel.requestMediaVideo()
        setObservers()
        validateInternet()
    }

    private fun setObservers() {
        binding.progressBar.visibility = View.VISIBLE
        viewModel.movie.observe(viewLifecycleOwner, {
            when (it.status) {
                FormatedResponse.Status.SUCCESS -> {
                    it.data?.let { safeMovie -> setMovieUi(safeMovie) }
//                    binding.progressBar.visibility = View.GONE
                }
                FormatedResponse.Status.ERROR -> {
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                    Timber.d(it.message)
                }
                FormatedResponse.Status.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
            }
        })

        viewModel.movieVideos.observe(this, {
            setupVideo(it)
        })
    }

    private fun setMovieUi(movie: MovieDetailResponse) {
        binding.title.text = movie.original_title
        binding.resume.text = movie.overview

        Glide.with(requireContext())
            .load(movie.getPosterUrl())
            .placeholder(R.drawable.ic_movie)
            .error(R.drawable.ic_movie)
            .fitCenter()
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(binding.posterImage)

        Glide.with(requireContext())
            .load(movie.getPosterUrl())
            .placeholder(R.drawable.ic_movie)
            .error(R.drawable.ic_movie)
            .fitCenter()
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(binding.ivVideo)
    }

    private fun setupVideo(videos: ArrayList<Video>) {

        if (videos.isNotEmpty()) {

            var video = videos.find { it.isTrailer() && it.urlPlayback != null }
            if (video == null) {
                video = videos.find { it.urlPlayback?.isNotBlank() == true }
            }

            if (video?.urlThumbnail != "") {
                Glide.with(requireContext())
                    .load(video?.urlThumbnail)
                    .placeholder(R.drawable.ic_movie)
                    .error(R.drawable.ic_movie)
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(binding.ivVideo)
            }

            video?.let { item ->
                binding.ivVideo.visibility = View.VISIBLE
                binding.ivPlay.visibility = View.VISIBLE
                binding.progressBar.visibility = View.GONE
                binding.ivVideo.setOnClickListener {
                    val intent = Intent(context, PlayerActivity::class.java)
                    intent.putExtra(PARAM_URL, item.urlPlayback)
                    context?.startActivity(intent)
                }
            }
        } else {
            binding.progressBar.visibility = View.GONE
        }
    }

    private fun validateInternet() {
        val cm = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true

        if(!isConnected) {
            showAlertInternet()
        }
    }

    private fun showAlertInternet() {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(getString(R.string.text_message))
        builder.setMessage(getString(R.string.text_not_internet))
        builder.setPositiveButton(getString(R.string.text_retry)) { dialog, _ ->
            run {
                viewModel.requestMediaInfo()
                viewModel.requestMediaVideo()
                setObservers()
                dialog.dismiss()
            }
        }
        builder.setNegativeButton(getString(R.string.text_cancel)) { dialog, _ ->
            binding.progressBar.visibility = View.GONE
            dialog.dismiss()
        }

        val dialog = builder.create()
        dialog.show()
    }

}