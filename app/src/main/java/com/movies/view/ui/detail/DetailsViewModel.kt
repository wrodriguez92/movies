package com.movies.view.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.movies.data.models.MovieDetailResponse
import com.movies.data.models.Video
import com.movies.data.repository.MovieRepository
import com.movies.utils.FormatedResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class DetailsViewModel @Inject constructor(
    private val movieRepository: MovieRepository
) : ViewModel() {

    var mediaId: Int = 0

    var movie: LiveData<FormatedResponse<MovieDetailResponse>> = MutableLiveData()

    var movieVideos: LiveData<ArrayList<Video>> = MutableLiveData()


    fun requestMediaInfo() {
        movie = movieRepository.getMovie(mediaId)
    }

    fun requestMediaVideo() {
        movieVideos = movieRepository.requestMovieVideos(mediaId)
    }

}