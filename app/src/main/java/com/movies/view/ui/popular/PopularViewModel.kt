package com.movies.view.ui.popular

import androidx.lifecycle.ViewModel
import com.movies.data.repository.MovieRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PopularViewModel @Inject constructor(
    private val movieRepository: MovieRepository
) : ViewModel() {
    private var moviePage: Int = 1
    val movies = movieRepository.getMoviesPopular(moviePage)
    val movies2 = movieRepository.getMoviesPopular(moviePage+1)
    val movies3 = movieRepository.getMoviesPopular(moviePage+2)
    val movies4 = movieRepository.getMoviesPopular(moviePage+3)

    fun getMoviesPopular(){
        moviePage+=1
        movieRepository.getMoviesPopular(moviePage)
    }
}