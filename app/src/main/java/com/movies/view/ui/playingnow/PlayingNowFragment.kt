package com.movies.view.ui.playingnow

import android.app.AlertDialog
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.movies.R
import com.movies.databinding.FragmentPlayingNowBinding
import com.movies.utils.FormatedResponse
import com.movies.utils.Utils.isLastItemDisplaying
import com.movies.view.adapter.MoviesPlayingNowAdapter
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class PlayingNowFragment : Fragment(), MoviesPlayingNowAdapter.ItemListener {

    private lateinit var viewModel: PlayingNowViewModel
    private var _binding: FragmentPlayingNowBinding? = null
    private lateinit var moviesAdapter: MoviesPlayingNowAdapter

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this)[PlayingNowViewModel::class.java]
        _binding = FragmentPlayingNowBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setRecyclerViews()
        setObservers()
        validateInternet()
    }

    private fun setRecyclerViews() {
        moviesAdapter = MoviesPlayingNowAdapter(this)
        binding.playingNowRecyclerView.layoutManager = GridLayoutManager(requireContext(), 2)
        binding.playingNowRecyclerView.adapter = moviesAdapter
        binding.playingNowRecyclerView.addOnScrollListener(object :
            RecyclerView.OnScrollListener() {
            override fun onScrolled(@NonNull recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (isLastItemDisplaying(recyclerView)) {
                    viewModel.getMoviesPlayingNow()
                }
            }
        })

    }

    private fun setObservers() {
        viewModel.movies.observe(viewLifecycleOwner, {
            when (it.status) {
                FormatedResponse.Status.SUCCESS -> {
                    if (!it.data.isNullOrEmpty()) moviesAdapter.setItems(ArrayList(it.data))
                    binding.progressBar.visibility = View.GONE
                }
                FormatedResponse.Status.ERROR -> {
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                    Timber.d(it.message)
                }
                FormatedResponse.Status.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
            }
        })

        viewModel.movies2.observe(viewLifecycleOwner, {
            when (it.status) {
                FormatedResponse.Status.SUCCESS -> {
                    if (!it.data.isNullOrEmpty()) moviesAdapter.setItems(ArrayList(it.data))
                }
                FormatedResponse.Status.ERROR -> {
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                    Timber.d(it.message)
                }
                FormatedResponse.Status.LOADING -> {
                }
            }
        })

        viewModel.movies3.observe(viewLifecycleOwner, {
            when (it.status) {
                FormatedResponse.Status.SUCCESS -> {
                    if (!it.data.isNullOrEmpty()) moviesAdapter.setItems(ArrayList(it.data))
                }
                FormatedResponse.Status.ERROR -> {
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                    Timber.d(it.message)
                }
                FormatedResponse.Status.LOADING -> {
                }
            }
        })

        viewModel.movies4.observe(viewLifecycleOwner, {
            when (it.status) {
                FormatedResponse.Status.SUCCESS -> {
                    if (!it.data.isNullOrEmpty()) moviesAdapter.setItems(ArrayList(it.data))
                }
                FormatedResponse.Status.ERROR -> {
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                    Timber.d(it.message)
                }
                FormatedResponse.Status.LOADING -> {
                }
            }
        })
    }

    private fun validateInternet() {
        val cm = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true

        if(!isConnected) {
            showAlertInternet()
        }
    }

    private fun showAlertInternet() {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(getString(R.string.text_message))
        builder.setMessage(getString(R.string.text_not_internet))
        builder.setPositiveButton(getString(R.string.text_retry)) { dialog, _ ->
            run {
                view?.let { it1 ->
                    Navigation.findNavController(it1).navigate(R.id.navigation_playing_now)
                }
                dialog.dismiss()
            }
        }
        builder.setNegativeButton(getString(R.string.text_cancel)) { dialog, _ ->
            dialog.dismiss()
        }

        val dialog = builder.create()
        dialog.show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onItemClicked(movieId: Int) {
        findNavController().navigate(
            R.id.action_detailFragment_to_detailFragment,
            bundleOf("movieId" to movieId)
        )
    }
}