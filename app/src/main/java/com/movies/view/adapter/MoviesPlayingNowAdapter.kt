package com.movies.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.github.florent37.glidepalette.BitmapPalette
import com.github.florent37.glidepalette.GlidePalette
import com.movies.R
import com.movies.data.models.MoviePlayingNow
import com.movies.databinding.ItemMovieBinding

class MoviesPlayingNowAdapter (private val listener: ItemListener) : RecyclerView.Adapter<MovieViewHolder>() {

    interface ItemListener {
        fun onItemClicked(movieId: Int)
    }

    private val items = ArrayList<MoviePlayingNow>()

    fun setItems(items: ArrayList<MoviePlayingNow>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val binding: ItemMovieBinding = ItemMovieBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MovieViewHolder(binding, listener)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) = holder.bind(items[position])
}

class MovieViewHolder(private val itemBinding: ItemMovieBinding, private val listener: MoviesPlayingNowAdapter.ItemListener) : RecyclerView.ViewHolder(itemBinding.root),
    View.OnClickListener {

    private lateinit var movie: MoviePlayingNow

    init {
        itemBinding.root.setOnClickListener(this)
    }

    @SuppressLint("SetTextI18n")
    fun bind(item: MoviePlayingNow) {
        this.movie = item

        itemBinding.movieTitle.text = item.original_title

        Glide.with(itemBinding.root)
            .load(item.getPosterUrl())
            .listener(
                GlidePalette.with(item.getPosterUrl())
                .use(BitmapPalette.Profile.VIBRANT)
                .intoBackground(itemBinding.movieTittleBackground)
                .crossfade(true))
            .placeholder(R.drawable.ic_movie)
            .error(R.drawable.ic_movie)
            .fitCenter()
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(itemBinding.movieImage)
    }

    override fun onClick(v: View?) {
        listener.onItemClicked(movieId = movie.id)
    }
}

