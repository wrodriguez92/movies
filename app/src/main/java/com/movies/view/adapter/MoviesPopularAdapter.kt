package com.movies.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.github.florent37.glidepalette.BitmapPalette
import com.github.florent37.glidepalette.GlidePalette
import com.movies.R
import com.movies.data.models.MoviePopular
import com.movies.databinding.ItemMovieBinding

class MoviesPopularAdapter (private val listener: ItemListener) : RecyclerView.Adapter<MoviePopularViewHolder>() {

    interface ItemListener {
        fun onItemClicked(movieId: Int)
    }

    private val items = ArrayList<MoviePopular>()

    fun setItems(items: ArrayList<MoviePopular>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviePopularViewHolder {
        val binding: ItemMovieBinding = ItemMovieBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MoviePopularViewHolder(binding, listener)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: MoviePopularViewHolder, position: Int) = holder.bind(items[position])
}

class MoviePopularViewHolder(private val itemBinding: ItemMovieBinding, private val listener: MoviesPopularAdapter.ItemListener) : RecyclerView.ViewHolder(itemBinding.root),
    View.OnClickListener {

    private lateinit var movie: MoviePopular

    init {
        itemBinding.root.setOnClickListener(this)
    }

    @SuppressLint("SetTextI18n")
    fun bind(item: MoviePopular) {
        this.movie = item

        itemBinding.movieTitle.text = item.original_title

        Glide.with(itemBinding.root)
            .load(item.getPosterUrl())
            .listener(
                GlidePalette.with(item.getPosterUrl())
                .use(BitmapPalette.Profile.VIBRANT)
                .intoBackground(itemBinding.movieTittleBackground)
                .crossfade(true))
            .placeholder(R.drawable.ic_movie)
            .error(R.drawable.ic_movie)
            .fitCenter()
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(itemBinding.movieImage)
    }

    override fun onClick(v: View?) {
        listener.onItemClicked(movieId = movie.id)
    }
}

