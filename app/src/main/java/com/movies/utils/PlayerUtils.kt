package com.movies.utils

import android.content.Context
import android.util.SparseArray
import androidx.lifecycle.MutableLiveData
import at.huber.youtubeExtractor.VideoMeta
import at.huber.youtubeExtractor.YouTubeExtractor
import at.huber.youtubeExtractor.YtFile
import com.movies.data.models.Video


object PlayerUtils {
    private var requestTotal: Int = 0
    private var requestCount: Int = 0

    private fun getYouTubeUrl(context: Context, url: String, callback: (urlVideo: String?, urlThumbnail: String?) -> Unit) {
        object : YouTubeExtractor(context) {
            override fun onExtractionComplete(p0: SparseArray<YtFile>?, p1: VideoMeta?) {
                if (p0 != null) {
                    val itag = 22
                    callback(p0.get(itag)?.url, p1?.mqImageUrl)
                } else {
                    callback("", "")
                }
            }

        }.extract(url, true, true)
    }

    fun requestVideoPlaybackData(context: Context, videos: MutableLiveData<ArrayList<Video>>){
        videos.value?.let { list ->
            requestTotal = list.size
            requestCount = 0

            list.forEach {
                getYouTubeUrl(context, it.getYouTubeUrl()){ video, thumbnail ->
                    it.urlPlayback = video
                    it.urlThumbnail = thumbnail?.replace("http://", "https://")
                    checkDataComplete(videos)
                }
            }
        }
    }

    private fun checkDataComplete(videos: MutableLiveData<ArrayList<Video>>){
        requestCount++

        if(requestCount % 3 == 0 || requestCount >= requestTotal){
            videos.notifyObserver()
        }
    }

    //Used to notify liveData's observers
    private fun <T> MutableLiveData<T>.notifyObserver() {
        this.value = this.value
    }
}