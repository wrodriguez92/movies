package com.movies.utils

import android.content.Context
import android.content.SharedPreferences
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

object Utils {

    private var pref: SharedPreferences? = null
    private var editor: SharedPreferences.Editor? = null
    private const val preferenceName: String = "userPreferences"

    fun isLastItemDisplaying(recyclerView: RecyclerView): Boolean {
        if (recyclerView.adapter!!.itemCount != 0) {
            val lastVisibleItemPosition =
                (recyclerView.layoutManager as LinearLayoutManager?)!!.findLastCompletelyVisibleItemPosition()
            if (lastVisibleItemPosition != RecyclerView.NO_POSITION && lastVisibleItemPosition == recyclerView.adapter!!
                    .itemCount - 1
            ) return true
        }
        return false
    }

    private fun sharedPreferences(context: Context): SharedPreferences {
        if (pref == null) {
            pref = context.getSharedPreferences(
                preferenceName,
                Context.MODE_PRIVATE
            )
        }
        return pref!!
    }

    fun getString(context: Context, key: String?, default: String?): String? {
        return sharedPreferences(context).getString(key, default)
    }


}