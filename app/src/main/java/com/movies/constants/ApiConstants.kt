package com.movies.constants

object ApiConstants {
    const val API_KEY = "d17ef62cd58190802e42b5ac656aefef"
    const val base_api_url: String = "https://api.themoviedb.org/3/"
    const val base_image_url: String = "https://image.tmdb.org/t/p/w500"
}